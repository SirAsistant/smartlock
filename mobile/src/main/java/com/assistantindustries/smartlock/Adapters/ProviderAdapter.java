package com.assistantindustries.smartlock.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.assistantindustries.smartlock.Delegates.ProviderButtonDelegate;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.ProviderCredentials;
import com.assistantindustries.smartlock.R;
import com.assistantindustries.smartlock.View.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by alvaro on 26/09/2015.
 */
public class ProviderAdapter extends RecyclerView.Adapter {
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_PROVIDER = 0;

    private final Context context;
    private final ProviderButtonDelegate providerButtonDelegate;
    private List<Object> items;
    private int mHeaderId;
    private int mProviderLayout=R.layout.row_provider;
    private boolean showHeader=false;

    public ProviderAdapter(Context context, int headerId, ProviderButtonDelegate delegate) {
        this.items = new ArrayList<>();
        this.items.add(new HeaderItem());
        this.context = context;
        this.mHeaderId = headerId;
        this.providerButtonDelegate=delegate;
    }

    public void setProviderLayout(int providerLayout) {
        this.mProviderLayout = providerLayout;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = items.get(position);
        if (item instanceof Provider) {
            return TYPE_PROVIDER;
        } else {
            if (item instanceof HeaderItem)
                return TYPE_HEADER;
        }
        return -1;
    }

    public void addAll(Collection<Provider> moreProviders) {
        this.items.addAll(moreProviders);
    }

    public void clear() {
        items.clear();
        items.add(new HeaderItem());
    }



    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = null;
        switch (viewType) {
            case TYPE_HEADER:
                row = LayoutInflater.from(parent.getContext()).inflate(mHeaderId, parent, false);
                return new HeaderViewHolder(row);
            case TYPE_PROVIDER:
                row = LayoutInflater.from(parent.getContext()).inflate(mProviderLayout, parent, false);
                return new ProviderViewHolder(row);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProviderViewHolder) {
            ProviderViewHolder providerViewHolder = (ProviderViewHolder) holder;
            final Provider item = (Provider) items.get(position);
            providerViewHolder.nameView.setText(item.getName());
            Picasso.with(context).load(item.getProfilePicUrl()).into(providerViewHolder.imageView);
            providerViewHolder.providerButton.setText(providerButtonDelegate.getText(item));
            providerViewHolder.providerButton.setEnabled(providerButtonDelegate.isEnabled(item));
            providerViewHolder.providerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    providerButtonDelegate.onClick(item);
                }
            });
        } else {
            if (holder instanceof HeaderViewHolder) {
                ((HeaderViewHolder) holder).noProvidersView.setVisibility(showHeader ? View.VISIBLE : View.GONE);
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<Object> getItems() {
        return items;
    }

    public void removeItem(int providerIndex) {
        items.remove(providerIndex);
        notifyDataSetChanged();
    }

    public static class ProviderViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imageView;
        public TextView nameView;
        public Button providerButton;

        public ProviderViewHolder(View providerView) {
            super(providerView);
            this.nameView = (TextView) providerView.findViewById(R.id.provider_name);
            this.imageView = (CircleImageView) providerView.findViewById(R.id.provider_image);
            this.providerButton= (Button) providerView.findViewById(R.id.providerButton);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        public View noProvidersView;

        public HeaderViewHolder(View headerView) {
            super(headerView);
            this.noProvidersView = headerView.findViewById(R.id.no_available_providers_view);
        }
    }

    private class HeaderItem {
    }

    public void setShowHeader(boolean showHeader) {
        this.showHeader = showHeader;
    }
}
