package com.assistantindustries.smartlock.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.assistantindustries.smartlock.Delegates.DoorButtonDelegate;
import com.assistantindustries.smartlock.Model.Door;
import com.assistantindustries.smartlock.Model.Token;
import com.assistantindustries.smartlock.R;
import com.assistantindustries.smartlock.Tools.Tools;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Asistant on 30/09/2015.
 */
public class TokensAdapter extends BaseAdapter {
    private List<Token> items;
    private LayoutInflater inflater;
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public static final SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
    private DoorButtonDelegate delegate;

    public TokensAdapter(LayoutInflater inflater,DoorButtonDelegate delegate) {
        this.items = new ArrayList<>();
        this.inflater = inflater;
        this.delegate=delegate;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Token getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Token token = getItem(position);
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.row_token, parent, false);
            RowViewHolder viewHolder = new RowViewHolder();
            viewHolder.tokenName = (TextView) view.findViewById(R.id.token_name);
            viewHolder.doorsLayout = (LinearLayout) view.findViewById(R.id.doors_container);
            viewHolder.fromLabel = (TextView) view.findViewById(R.id.from_label);
            viewHolder.toLabel = (TextView) view.findViewById(R.id.to_label);
            viewHolder.repeatLabel = (TextView) view.findViewById(R.id.repeat_label);
            viewHolder.usesLabel = (TextView) view.findViewById(R.id.uses_label);
            viewHolder.repeatLayout= (LinearLayout) view.findViewById(R.id.repeat_row);
            viewHolder.doorHolders = new ArrayList<>();
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }
        RowViewHolder viewHolder = (RowViewHolder) view.getTag();
        int layoutCount = viewHolder.doorsLayout.getChildCount();
        int doorCount = token.getDoors().size();
        while (layoutCount < doorCount) {
            View doorView = inflater.inflate(R.layout.row_door, null, false);
            DoorViewHolder doorHolder = new DoorViewHolder();
            doorHolder.doorName = (TextView) doorView.findViewById(R.id.door_name);
            doorHolder.openButton = (Button) doorView.findViewById(R.id.open_button);
            viewHolder.doorsLayout.addView(doorView);
            viewHolder.doorHolders.add(doorHolder);
            layoutCount++;
        }
        while (layoutCount > doorCount) {
            viewHolder.doorsLayout.removeViewAt(layoutCount - 1);
            viewHolder.doorHolders.remove(layoutCount - 1);
            layoutCount--;
        }
        for (int i = 0; i < token.getDoors().size(); i++) {
            final String door = token.getDoors().get(i);
            DoorViewHolder doorHolder = viewHolder.doorHolders.get(i);
            doorHolder.doorName.setText(door);
            doorHolder.openButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delegate.onClick(door,token);
                }
            });
            doorHolder.openButton.setEnabled(delegate.isEnabled(door,token));
        }
        viewHolder.tokenName.setText(token.getName());

        viewHolder.fromLabel.setText((token.getValidity().isDaily() ? hourFormat : dateFormat).format(token.getValidity().getFrom()));
        viewHolder.toLabel.setText((token.getValidity().isDaily() ? hourFormat : dateFormat).format(token.getValidity().getTo()));

        if(token.getValidity().isDaily()){
            String[] repeatStrings=new String[token.getValidity().getFormattedRepeat().size()];
            List<Integer> sortedDays = new ArrayList<>(token.getValidity().getFormattedRepeat());
            Collections.sort(sortedDays);
            for(int i=0;i<sortedDays.size();i++){
                Date dateAtThatDayOfWeek=new DateTime().withDayOfWeek(sortedDays.get(i)).toDate();
                SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                repeatStrings[i]=Tools.capitalize(sdf.format(dateAtThatDayOfWeek));
            }
            viewHolder.repeatLabel.setText(Tools.joinStrings(repeatStrings,", "));
        }else{
            viewHolder.repeatLayout.setVisibility(View.GONE);
        }

        if (token.getValidity().getUses() < 0)
            viewHolder.usesLabel.setText(parent.getContext().getString(R.string.unlimited));
        else
            viewHolder.usesLabel.setText("" + token.getValidity().getUses());
        return view;
    }

    public void clear() {
        items.clear();
    }

    public void addAll(List<Token> tokens) {
        items.addAll(tokens);
    }

    private class RowViewHolder {
        TextView tokenName;
        LinearLayout doorsLayout;
        TextView fromLabel;
        TextView toLabel;
        TextView repeatLabel;
        TextView usesLabel;
        List<DoorViewHolder> doorHolders;
        LinearLayout repeatLayout;
    }

    private class DoorViewHolder {
        TextView doorName;
        Button openButton;
    }
}
