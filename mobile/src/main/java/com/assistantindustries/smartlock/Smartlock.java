package com.assistantindustries.smartlock;

import android.app.Application;

import com.orm.SugarApp;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tsums.androidcookiejar.PersistentCookieStore;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * Created by assistant on 13/9/16.
 */
public class Smartlock extends SugarApp {

    @Override
    public void onCreate() {
        super.onCreate();

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        Picasso.setSingletonInstance(built);

        CookieHandler.setDefault(
                new CookieManager(
                        PersistentCookieStore.getInstance(this),
                        CookiePolicy.ACCEPT_ORIGINAL_SERVER));
    }

}