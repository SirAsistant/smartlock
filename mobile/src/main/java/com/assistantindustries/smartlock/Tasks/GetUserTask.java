package com.assistantindustries.smartlock.Tasks;

import android.os.AsyncTask;

import com.assistantindustries.smartlock.Listeners.OnGetUserAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnLoginAttemptedListener;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tools.HttpResponse;
import com.assistantindustries.smartlock.Tools.RestClient;
import com.assistantindustries.smartlock.Tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

/**
 * Created by Asistant on 29/09/2015.
 */
public class GetUserTask extends AsyncTask<Void, Void, HttpResponse> {
    private String serverAddress;
    private OnGetUserAttemptedListener listener;
    private String id;

    public GetUserTask(String serverAddress, String id, OnGetUserAttemptedListener listener) {
        this.serverAddress = serverAddress;
        this.listener = listener;
        this.id = id;
    }

    @Override
    protected HttpResponse doInBackground(Void... params) {
        RestClient client = new RestClient(serverAddress + "/api/mobile/info/" + id, RestClient.GET_METHOD);
        client.execute();
        return client.getResponse();
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);
        if (httpResponse != null) {
            listener.onGetUserAttempted(httpResponse.getResultCode() > 0 && httpResponse.getResultCode() < 500, httpResponse.getResultCode() == 200, User.fromJsonString(httpResponse.getResponseString()));
        } else {
            listener.onGetUserAttempted(false, false, null);
        }
    }
}
