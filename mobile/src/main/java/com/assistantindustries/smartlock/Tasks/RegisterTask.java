package com.assistantindustries.smartlock.Tasks;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.assistantindustries.smartlock.Listeners.OnRegisterAttemptedListener;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tools.HttpRequest;
import com.assistantindustries.smartlock.Tools.HttpResponse;
import com.assistantindustries.smartlock.Tools.Tools;

import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by alvaro on 27/09/2015.
 */
public class RegisterTask extends AsyncTask<Void, Void, HttpResponse> {
    private final OnRegisterAttemptedListener listener;
    private InputStream photoStream;
    private String serverAddress;
    private User user;

    public RegisterTask(String serverAddress, InputStream photoStream, User user, OnRegisterAttemptedListener listener) {
        this.serverAddress = serverAddress;
        this.photoStream = photoStream;
        this.user = user;
        this.listener = listener;
    }

    @Override
    protected HttpResponse doInBackground(Void... params) {
        HttpRequest request = HttpRequest.post(serverAddress + "/api/mobile/newuser");

        request.part("profilePic", "profilePic", "image/jpg", photoStream);

        if (Tools.validString(user.getName()))
            request.part("name", user.getName());
        if (Tools.validString(user.getAlias()))
            request.part("alias", user.getAlias());
        if (Tools.validString(user.getPassword()))
            request.part("password", user.getPassword());
        if (Tools.validString(user.getEmail()))
            request.part("email", user.getEmail());

        int code = request.code();
        String responseBody = request.body();

        return new HttpResponse(responseBody, responseBody, code, code == 200);
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);
        listener.onRegisterAttempted(httpResponse.getResultCode() != 0, httpResponse.isAchieved(),
                Tools.validString(httpResponse.getResponseString()) ?
                        User.fromJsonString(httpResponse.getResponseString()) : new User(), httpResponse.getError());
    }
}
