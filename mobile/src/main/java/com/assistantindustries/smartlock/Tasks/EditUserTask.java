package com.assistantindustries.smartlock.Tasks;

import android.os.AsyncTask;

import com.assistantindustries.smartlock.Listeners.OnEditUserAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnRegisterAttemptedListener;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tools.HttpRequest;
import com.assistantindustries.smartlock.Tools.HttpResponse;
import com.assistantindustries.smartlock.Tools.Tools;

import java.io.InputStream;

/**
 * Created by assistant on 21/9/16.
 */

public class EditUserTask extends AsyncTask<Void, Void, HttpResponse> {
    private final OnEditUserAttemptedListener listener;
    private InputStream photoStream;
    private String serverAddress;
    private User user;

    public EditUserTask(String serverAddress, InputStream photoStream, User user, OnEditUserAttemptedListener listener) {
        this.serverAddress = serverAddress;
        this.photoStream = photoStream;
        this.user = user;
        this.listener = listener;
    }

    @Override
    protected HttpResponse doInBackground(Void... params) {
        HttpRequest request = HttpRequest.post(serverAddress + "/api/mobile/info/"+user.getIdentifier());

        if (photoStream != null)
            request.part("profilePic", "profilePic", "image/jpg", photoStream);

        if (Tools.validString(user.getName()))
            request.part("name", user.getName());
        if (Tools.validString(user.getAlias()))
            request.part("alias", user.getAlias());
        if (Tools.validString(user.getPassword()))
            request.part("password", user.getPassword());
        if (Tools.validString(user.getEmail()))
            request.part("email", user.getEmail());

        int code = request.code();
        String responseBody = request.body();

        return new HttpResponse(responseBody, responseBody, code, code == 200);
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);
        listener.onEditUserAttempted(httpResponse.getResultCode() != 0, httpResponse.isAchieved());
    }
}
