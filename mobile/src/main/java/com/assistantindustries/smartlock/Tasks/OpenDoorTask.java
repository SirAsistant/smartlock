package com.assistantindustries.smartlock.Tasks;

import android.os.AsyncTask;

import com.assistantindustries.smartlock.Listeners.OnOpenDoorAttemptedListener;
import com.assistantindustries.smartlock.Tools.HttpResponse;
import com.assistantindustries.smartlock.Tools.RestClient;
import com.assistantindustries.smartlock.Tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Asistant on 10/10/2015.
 */
public class OpenDoorTask extends AsyncTask<Void,Void,HttpResponse>{
    private OnOpenDoorAttemptedListener listener;
    private String serverAddress;
    private String userId;
    private String doorName;
    private String tokenId;

    public OpenDoorTask( String serverAddress, String userId, String doorName, String tokenId,OnOpenDoorAttemptedListener listener) {
        this.listener = listener;
        this.serverAddress = serverAddress;
        this.userId = userId;
        this.doorName = doorName;
        this.tokenId = tokenId;
    }

    @Override
    protected HttpResponse doInBackground(Void... params) {
        JSONObject object = new JSONObject();
        try {
            object.put("user", userId);
            object.put("door", doorName);
            object.put("token", tokenId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RestClient client = new RestClient(serverAddress + "/api/mobile/open", RestClient.POST_METHOD, object.toString());
        Tools.addJsonHeaders(client);
        client.execute();
        HttpResponse response = client.getResponse();
        if (response.getResultCode() == 200)
            return response;
        else
            return null;
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);
        if(httpResponse!=null){
            listener.onOpenDoorAttempted(true,httpResponse.getResultCode()==200,httpResponse.getError());
        }else{
            listener.onOpenDoorAttempted(false,false,"");
        }
    }
}
