package com.assistantindustries.smartlock.Tasks;

import android.os.AsyncTask;

import com.assistantindustries.smartlock.ApplicationData;
import com.assistantindustries.smartlock.Listeners.OnProvidersRetrievedListener;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Tools.HttpResponse;
import com.assistantindustries.smartlock.Tools.JsonTools;
import com.assistantindustries.smartlock.Tools.RestClient;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Created by Asistant on 08/07/2015.
 */
public class GetProvidersTask extends AsyncTask<Void, Void, List<Provider>> {

    private final OnProvidersRetrievedListener listener;

    public GetProvidersTask(OnProvidersRetrievedListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<Provider> doInBackground(Void... params) {
        RestClient client = new RestClient(ApplicationData.MAIN_SERVER_ADDRESS, RestClient.GET_METHOD);
        client.execute();
        HttpResponse response = client.getResponse();
        if (response.getResultCode() == 200) {
            try {
                return JsonTools.getProviderArray(new JSONArray(response.getResponseString()));
            } catch (JSONException ignored) {
                return null;
            }
        } else
            return null;
    }

    @Override
    protected void onPostExecute(List<Provider> providers) {
        super.onPostExecute(providers);
        listener.handleRetrievedProviders(providers);
    }

}
