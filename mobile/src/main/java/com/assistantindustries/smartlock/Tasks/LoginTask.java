package com.assistantindustries.smartlock.Tasks;

import android.os.AsyncTask;

import com.assistantindustries.smartlock.Listeners.OnLoginAttemptedListener;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tools.HttpResponse;
import com.assistantindustries.smartlock.Tools.RestClient;
import com.assistantindustries.smartlock.Tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alvaro on 27/09/2015.
 */
public class LoginTask extends AsyncTask<Void, Void, HttpResponse> {
    private final OnLoginAttemptedListener listener;
    private final String alias;
    private final String password;
    private String serverAddress;

    public LoginTask(String serverAddress, OnLoginAttemptedListener listener, String alias, String password) {
        this.serverAddress = serverAddress;
        this.listener = listener;
        this.alias = alias;
        this.password = password;
    }

    @Override
    protected HttpResponse doInBackground(Void... params) {
        JSONObject object = new JSONObject();
        try {
            object.put("alias", alias);
            object.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RestClient client = new RestClient(serverAddress + "/api/mobile/userlogin", RestClient.POST_METHOD, object.toString());
        Tools.addJsonHeaders(client);
        client.execute();
        return client.getResponse();
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse) {
        super.onPostExecute(httpResponse);
        listener.onLoginAttempted(httpResponse.getResultCode() != -1, httpResponse.getResultCode() == 200, User.fromJsonString(httpResponse.getResponseString()), httpResponse.getError());

    }
}
