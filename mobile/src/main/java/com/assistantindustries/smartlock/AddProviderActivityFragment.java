package com.assistantindustries.smartlock;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.assistantindustries.smartlock.Adapters.ProviderAdapter;
import com.assistantindustries.smartlock.Delegates.ProviderButtonDelegate;
import com.assistantindustries.smartlock.Listeners.OnProvidersRetrievedListener;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.ProviderCredentials;
import com.assistantindustries.smartlock.Tasks.GetProvidersTask;
import com.assistantindustries.smartlock.Tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class AddProviderActivityFragment extends Fragment {


    private RecyclerView mProviderList;
    private LinearLayoutManager mLayoutManager;
    private ProviderAdapter mProviderAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private List<ProviderCredentials> mAddedProvidersList;

    public AddProviderActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_provider, container, false);
        mProviderList = (RecyclerView) rootView.findViewById(R.id.provider_list);
        mProviderList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());

        mProviderList.setLayoutManager(mLayoutManager);
        mAddedProvidersList = ProviderCredentials.listAll(ProviderCredentials.class);
        mProviderAdapter = new ProviderAdapter(getContext(), R.layout.header_no_providers_available, new ProviderButtonDelegate() {
            @Override
            public boolean isEnabled(Provider provider) {
                return ProviderCredentials.findByProviderName(provider.getName(), mAddedProvidersList) == null;
            }

            @Override
            public String getText(Provider provider) {
                if (ProviderCredentials.findByProviderName(provider.getName(), mAddedProvidersList) == null) {
                    return getString(R.string.add);
                } else {
                    return getString(R.string.added_tick);
                }
            }

            @Override
            public void onClick(Provider provider) {
                Intent i = new Intent(getActivity(), ProviderAuthActivity.class);
                i.putExtra(AddProviderActivity.EXTRA_PROVIDER, provider.toJson().toString());
                startActivityForResult(i, AddProviderActivity.REQUEST_AUTH);
            }
        });
        mProviderList.setAdapter(mProviderAdapter);
        reloadProviders();
        mRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mRefreshLayout.setColorSchemeColors(Tools.getRefreshColors());
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadProviders();
            }
        });
        return rootView;
    }

    private void reloadProviders() {
        GetProvidersTask task = new GetProvidersTask(new OnProvidersRetrievedListener() {
            @Override
            public void handleRetrievedProviders(List<Provider> providers) {
                mRefreshLayout.setRefreshing(false);
                if (providers != null) {
                    mProviderAdapter.clear();
                    mProviderAdapter.addAll(providers);
                    mProviderAdapter.notifyDataSetChanged();
                }else{
                    Snackbar.make(mProviderList, getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case AddProviderActivity.REQUEST_AUTH:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Provider authTarget = Provider.fromJson(new JSONObject(data.getStringExtra(AddProviderActivity.EXTRA_PROVIDER)));
                        ProviderCredentials credentials = new ProviderCredentials(data.getStringExtra(AddProviderActivity.EXTRA_USER_TOKEN), data.getStringExtra(AddProviderActivity.EXTRA_USER_ID), authTarget.getIdentifier(),authTarget.getName());
                        credentials.save();
                        List<ProviderCredentials> addedProviders = ProviderCredentials.listAll(ProviderCredentials.class);
                        mAddedProvidersList.clear();
                        mAddedProvidersList.addAll(addedProviders);
                        mProviderAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
