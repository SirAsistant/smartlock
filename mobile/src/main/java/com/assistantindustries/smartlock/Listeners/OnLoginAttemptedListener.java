package com.assistantindustries.smartlock.Listeners;

import com.assistantindustries.smartlock.Model.User;

/**
 * Created by alvaro on 27/09/2015.
 */
public interface OnLoginAttemptedListener {
    void onLoginAttempted(boolean attempted, boolean success, User user, String error);
}
