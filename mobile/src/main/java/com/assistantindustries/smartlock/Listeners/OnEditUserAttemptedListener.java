package com.assistantindustries.smartlock.Listeners;

import com.assistantindustries.smartlock.Model.User;

/**
 * Created by assistant on 21/9/16.
 */

public interface OnEditUserAttemptedListener {
    public void onEditUserAttempted(boolean attempted, boolean success);
}
