package com.assistantindustries.smartlock.Listeners;

import com.assistantindustries.smartlock.Model.User;

/**
 * Created by Asistant on 10/10/2015.
 */
public interface OnOpenDoorAttemptedListener {
    void onOpenDoorAttempted(boolean attempted, boolean success, String error);
}
