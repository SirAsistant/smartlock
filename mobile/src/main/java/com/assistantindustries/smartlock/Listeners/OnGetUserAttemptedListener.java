package com.assistantindustries.smartlock.Listeners;

import com.assistantindustries.smartlock.Model.User;

/**
 * Created by Asistant on 29/09/2015.
 */
public interface OnGetUserAttemptedListener {
    void onGetUserAttempted(boolean connected,boolean achieved,User user);
}
