package com.assistantindustries.smartlock.Listeners;


import com.assistantindustries.smartlock.Model.Provider;

import java.util.List;

/**
 * Created by Asistant on 08/07/2015.
 */
public interface OnProvidersRetrievedListener {
    void handleRetrievedProviders(List<Provider> providers);
}
