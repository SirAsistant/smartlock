package com.assistantindustries.smartlock;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.assistantindustries.smartlock.Adapters.ProviderAdapter;
import com.assistantindustries.smartlock.Delegates.ProviderButtonDelegate;
import com.assistantindustries.smartlock.Listeners.OnGetUserAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnProvidersRetrievedListener;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.ProviderCredentials;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tasks.GetProvidersTask;
import com.assistantindustries.smartlock.Tasks.GetUserTask;
import com.assistantindustries.smartlock.Tools.AsyncParallel;
import com.assistantindustries.smartlock.Tools.ItemClickSupport;
import com.assistantindustries.smartlock.Tools.Tools;
import com.melnykov.fab.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProviderListActivityFragment extends Fragment {

    private RecyclerView mProviderList;
    private LinearLayoutManager mLayoutManager;
    private ProviderAdapter mProviderAdapter;
    private List<ProviderCredentials> addedProviders;
    private SwipeRefreshLayout mRefreshLayout;
    private ProgressBar loadingIndicator;

    public ProviderListActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_provider_list, container, false);
        mProviderList = (RecyclerView) rootView.findViewById(R.id.provider_list);
        mRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        loadingIndicator= (ProgressBar) rootView.findViewById(R.id.progress_bar);
        mRefreshLayout.setColorSchemeColors(Tools.getRefreshColors());
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mProviderList.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mProviderList.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mProviderAdapter = new ProviderAdapter(getContext(), R.layout.header_no_added_providers,
        new ProviderButtonDelegate() {
            @Override
            public boolean isEnabled(Provider provider) {
                return true;
            }

            @Override
            public String getText(Provider provider) {
              return getString(R.string.remove);
            }

            @Override
            public void onClick(final Provider provider) {
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.delete_provider))
                        .setMessage(getString(R.string.do_you_want_to_delete_provider))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ProviderCredentials credentials=ProviderCredentials.findByProviderName(provider.getName());
                                if(credentials!=null){
                                    credentials.delete();
                                    int providerIndex=mProviderAdapter.getItems().indexOf(provider);
                                    mProviderAdapter.removeItem(providerIndex);
                                }
                            }

                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
            }
        });
        mProviderAdapter.setProviderLayout(R.layout.row_provider_card);
        mProviderList.setAdapter(mProviderAdapter);
        mProviderAdapter.addAll(Provider.listAll(Provider.class));
        mProviderAdapter.notifyDataSetChanged();
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.attachToRecyclerView(mProviderList);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddProviderActivity.class);
                startActivity(i);
            }
        });

        ItemClickSupport.addTo(mProviderList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (position > 0) {
                    Provider provider = (Provider) mProviderAdapter.getItem(position);
                    List<ProviderCredentials> addedProviderCredentials = ProviderCredentials.listAll(ProviderCredentials.class);
                    for (ProviderCredentials credentials : addedProviderCredentials) {
                        if (provider.getName().equals(credentials.getProviderName())) {
                            Intent i = new Intent(getActivity(), ProviderActivity.class);
                            i.putExtra(ProviderActivity.EXTRA_PROVIDER_NAME, credentials.getProviderName());
                            startActivity(i);
                            break;
                        }
                    }
                }
            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateAddedProviders();
            }
        });
        mProviderAdapter.setShowHeader(false);
        setShowLoadingIndicator(true);
        return rootView;
    }

    private void setShowLoadingIndicator(boolean value) {
        loadingIndicator.setVisibility(value ? View.VISIBLE : View.GONE);
    }

    private void updateAddedProviders() {

        GetProvidersTask task = new GetProvidersTask(new OnProvidersRetrievedListener() {
            @Override
            public void handleRetrievedProviders(List<Provider> providers) {

                if (providers != null) {
                    final List<Provider> addedProviders = new ArrayList<>();
                    List<ProviderCredentials> addedProvidersCredentials = ProviderCredentials.listAll(ProviderCredentials.class);
                    mProviderAdapter.clear();

                    List<GetUserTask> providersToTest = new ArrayList<>();
                    final AsyncParallel testProviders = new AsyncParallel();

                    for (final Provider provider : providers) {
                        for (final ProviderCredentials credentials : addedProvidersCredentials) {
                            if (credentials.getProviderName().equals(provider.getName())) {
                                GetUserTask getUserTask = new GetUserTask(provider.getUrl(), credentials.getIdentifier(), new OnGetUserAttemptedListener() {
                                    @Override
                                    public void onGetUserAttempted(boolean connected,boolean achieved, User user) {
                                        if (achieved && user != null) {
                                            addedProviders.add(provider);
                                        }else{
                                            if(connected&&!achieved){
                                                credentials.delete();
                                            }
                                        }
                                        testProviders.notifyFinish(false);
                                    }
                                });
                                providersToTest.add(getUserTask);
                            }
                        }
                    }

                    testProviders.start(providersToTest.size(),new AsyncParallel.FinishCallback() {
                        @Override
                        public void onEnd(boolean error) {
                            setShowLoadingIndicator(false);
                            mProviderAdapter.setShowHeader(addedProviders.size()==0);
                            mProviderAdapter.addAll(addedProviders);
                            mProviderAdapter.notifyDataSetChanged();
                            if(mRefreshLayout!=null)
                                mRefreshLayout.setRefreshing(false);
                        }
                    });

                    for(GetUserTask getUserTask:providersToTest){
                        getUserTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

                }else{
                    Snackbar.make(mProviderList, getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private boolean isProviderAdded(Provider provider){
        for(ProviderCredentials credentials: addedProviders){
           if( credentials.getProviderName().equals(provider.getName()))
               return true;
        }
        return false;
    }

    @Override
    public void onStart() {
        updateAddedProviders();
        super.onStart();
    }
}
