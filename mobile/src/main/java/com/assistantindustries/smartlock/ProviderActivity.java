package com.assistantindustries.smartlock;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.assistantindustries.smartlock.Adapters.TokensAdapter;
import com.assistantindustries.smartlock.Delegates.DoorButtonDelegate;
import com.assistantindustries.smartlock.Listeners.OnGetUserAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnOpenDoorAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnProvidersRetrievedListener;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.ProviderCredentials;
import com.assistantindustries.smartlock.Model.Token;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tasks.GetProvidersTask;
import com.assistantindustries.smartlock.Tasks.GetUserTask;
import com.assistantindustries.smartlock.Tasks.OpenDoorTask;
import com.assistantindustries.smartlock.Tools.Tools;
import com.assistantindustries.smartlock.View.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

public class ProviderActivity extends AppCompatActivity {

    public static final String EXTRA_PROVIDER_NAME = "PROVIDER_ID";
    private static final int EDIT_PROFILE = 1;

    private TextView providerName;
    private CircleImageView userImage;
    private TextView aliasView;
    private TextView nameView;
    private TextView mailView;
    private String mProviderName;
    private ProviderCredentials mCredentials;
    private ListView mListView;
    private TokensAdapter mTokensAdapter;
    private TextView emptyTokensText;
    private View deactivatedView;

    private User mUser;
    private Provider mProvider;
    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mProviderName = getIntent().getStringExtra(EXTRA_PROVIDER_NAME);
        if (mProviderName == null)
            mProviderName = savedInstanceState.getString(EXTRA_PROVIDER_NAME);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);
        loadCredentials();
        View header=getLayoutInflater().inflate(R.layout.header_provider,null, false);
        findViews(header);
        mListView.addHeaderView(header);
        mTokensAdapter=new TokensAdapter(getLayoutInflater(), new DoorButtonDelegate() {
            @Override
            public void onClick(String door,Token token) {
                OpenDoorTask task=new OpenDoorTask(mProvider.getUrl(),mUser.getIdentifier(),door,token.getIdentifier(), new OnOpenDoorAttemptedListener() {
                    @Override
                    public void onOpenDoorAttempted(boolean attempted, boolean success, String error) {
                        if(success&&attempted){
                            Snackbar.make(mListView, getString(R.string.door_has_been_opened), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            loadUserInfo(mProvider);
                        }else{
                            Snackbar.make(mListView,error, Snackbar.LENGTH_SHORT)
                                    .setAction("Action", null).show();
                            loadUserInfo(mProvider);
                        }
                    }
                });
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public boolean isEnabled(String door, Token token) {
                return mUser!=null && mUser.getActive() && token.getValidity().isValidAt(new Date()) && token.getValidity().getUses()!=0;
            }
        });
        mListView.setAdapter(mTokensAdapter);
        loadProviderInfo();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadUserInfo(mProvider);
            }
        });
        refreshLayout.setColorSchemeColors(Tools.getRefreshColors());
    }

    private void loadCredentials() {
        List<ProviderCredentials> addedProvidersCredentials = ProviderCredentials.listAll(ProviderCredentials.class);
        for (ProviderCredentials credentials : addedProvidersCredentials) {
            if (credentials.getProviderName().equals(mProviderName))
                mCredentials = credentials;
        }
    }

    private void loadProviderInfo() {
        GetProvidersTask task = new GetProvidersTask(new OnProvidersRetrievedListener() {
            @Override
            public void handleRetrievedProviders(List<Provider> providers) {
                if (providers != null) {
                    for (final Provider provider : providers) {
                        if (provider.getName().equals(mProviderName)) {
                            mProvider=provider;
                            getSupportActionBar().setTitle(provider.getName());
                            loadUserInfo(provider);
                            providerName.setText(provider.getName());
                            break;
                        }
                    }
                }else{
                    Snackbar.make(mListView,getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadUserInfo(Provider provider) {
        GetUserTask getUserTask = new GetUserTask(provider.getUrl(), mCredentials.getIdentifier(), new OnGetUserAttemptedListener() {
            @Override
            public void onGetUserAttempted(boolean connected,boolean achieved, User user) {
                if (achieved&&user!=null) {
                    mUser=user;
                    emptyTokensText.setVisibility(user.getTokens().size() > 0 ? View.GONE : View.VISIBLE);
                    mTokensAdapter.clear();
                    mTokensAdapter.addAll(user.getTokens());
                    mTokensAdapter.notifyDataSetChanged();
                    aliasView.setText(Tools.validString(user.getAlias()) ? user.getAlias() : getResources().getString(R.string.empty_alias));
                    nameView.setText(Tools.validString(user.getName())?user.getName():getResources().getString(R.string.empty_name));
                    mailView.setText(Tools.validString(user.getEmail()) ? user.getEmail() : getResources().getString(R.string.empty_email));
                    Picasso.with(ProviderActivity.this).load(user.getProfilePic()).into(userImage);
                    deactivatedView.setVisibility(user.getActive() ? View.GONE : View.VISIBLE);
                }else{
                    Snackbar.make(mListView,getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                }
                if(refreshLayout!=null){
                    refreshLayout.setRefreshing(false);
                }
            }
        });
        getUserTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void findViews(View header) {
        providerName = (TextView) header.findViewById(R.id.provider_name);
        userImage = (CircleImageView) header.findViewById(R.id.user_logo);
        aliasView = (TextView) header.findViewById(R.id.aliasLabel);
        nameView = (TextView) header.findViewById(R.id.nameLabel);
        mailView = (TextView) header.findViewById(R.id.mailLabel);
        mListView= (ListView) findViewById(R.id.tokens_list);
        emptyTokensText=(TextView) header.findViewById(R.id.empty_tokens_text);
        deactivatedView=findViewById(R.id.deactivated_view);
        refreshLayout= (SwipeRefreshLayout) findViewById(R.id.swipe_container);
    }

    public static float clamp(float value, float max, float min) {
        return Math.max(Math.min(value, min), max);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_provider, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case R.id.action_edit_profile:
                Intent i = new Intent(ProviderActivity.this,EditProfileActivity.class);
                i.putExtra(EditProfileActivityFragment.PROVIDER_NAME,mProviderName);
                startActivityForResult(i,EDIT_PROFILE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case EDIT_PROFILE:
                loadUserInfo(mProvider);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(EXTRA_PROVIDER_NAME,mProviderName);
        super.onSaveInstanceState(outState);
    }
}
