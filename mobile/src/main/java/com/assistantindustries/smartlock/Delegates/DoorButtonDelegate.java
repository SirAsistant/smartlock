package com.assistantindustries.smartlock.Delegates;

import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.Token;

/**
 * Created by Asistant on 10/10/2015.
 */
public interface DoorButtonDelegate {
    void onClick(String door,Token token);
    boolean isEnabled(String door,Token token);
}
