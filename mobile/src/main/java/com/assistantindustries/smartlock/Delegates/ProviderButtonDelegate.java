package com.assistantindustries.smartlock.Delegates;

import android.view.View;

import com.assistantindustries.smartlock.Model.Provider;

/**
 * Created by Asistant on 06/10/2015.
 */
public interface ProviderButtonDelegate {
    boolean isEnabled(Provider provider);
    String getText(Provider provider);
    void onClick(Provider provider);
}
