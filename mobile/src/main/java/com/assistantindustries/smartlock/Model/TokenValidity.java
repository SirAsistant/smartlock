package com.assistantindustries.smartlock.Model;

import com.assistantindustries.smartlock.Tools.JsonTools;
import com.assistantindustries.smartlock.Tools.Tools;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by alvaro on 27/09/2015.
 */
public class TokenValidity {
    Date from;
    Date to;
    List<Integer> repeat;
    int uses;


    public TokenValidity(Date from, Date to, List<Integer> repeat, int uses) {
        this.from = from;
        this.to = to;
        this.repeat = repeat;
        this.uses = uses;
    }

    public static TokenValidity fromJsonString(String jsonString) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonObject != null) {
            Date from = JsonTools.getDateIgnoringException(JsonTools.getStringIgnoringException(jsonObject, "from"));
            Date to = JsonTools.getDateIgnoringException(JsonTools.getStringIgnoringException(jsonObject, "to"));
            JSONArray jsonRepeat = JsonTools.getJSONArrayIgnoringException(jsonObject, "repeat");
            List<Integer> repeat = new ArrayList<>();
            if (jsonRepeat != null) {
                for (int i = 0; i < jsonRepeat.length(); i++) {
                    int repeatItem = -1;
                    try {
                        repeatItem = jsonRepeat.getInt(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (repeatItem != -1)
                        repeat.add(repeatItem);
                }
            }
            int uses = JsonTools.getIntegerIgnoringException(jsonObject, "uses");
            return new TokenValidity(from, to, repeat, uses);
        }
        return null;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<Integer> getRepeat() {
        return repeat;
    }

    public void setRepeat(List<Integer> repeat) {
        this.repeat = repeat;
    }

    public int getUses() {
        return uses;
    }

    public void setUses(int uses) {
        this.uses = uses;
    }

    public boolean isDaily() {
        return repeat.size() > 0;
    }

    public boolean isValidAt(Date current){
        DateTime dateTime=new DateTime(current);

        LocalTime toTime=new LocalTime(to);
        LocalTime fromTime =new LocalTime(from);
        LocalTime currentTime=new LocalTime(current);
        if(isDaily()){
            for(Integer repeatDay:getFormattedRepeat()){
                if(repeatDay==dateTime.getDayOfWeek()){
                    return currentTime.isAfter(fromTime)&&toTime.isAfter(currentTime);
                }
            }
        }else{
            return current.after(from)&&to.after(current);
        }
        return false;
    }

    public Set<Integer> getFormattedRepeat() {
        Set<Integer> formattedDays=new HashSet<>();
        for(Integer repeatDay:repeat){
            formattedDays.add(Tools.shiftInteger(repeatDay, -1, 0, 7)+1);
        }
        return formattedDays;
    }
}
