package com.assistantindustries.smartlock.Model;

import com.assistantindustries.smartlock.Tools.JsonTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvaro on 27/09/2015.
 */
public class Token {
    String name;
    TokenValidity validity;
    List<String> doors;
    String identifier;

    public Token(String name, TokenValidity validity, List<String> doors,String identifier) {
        this.name = name;
        this.validity = validity;
        this.doors = doors;
        this.identifier=identifier;
    }

    public static Token fromJsonString(String jsonString) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonObject != null) {
            String name = JsonTools.getStringIgnoringException(jsonObject, "name");
            String identifier = JsonTools.getStringIgnoringException(jsonObject, "_id");
            TokenValidity validity = null;
            try {
                validity = TokenValidity.fromJsonString(jsonObject.getJSONObject("validity").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray jsonDoors = JsonTools.getJSONArrayIgnoringException(jsonObject, "doors");
            List<String> doors = new ArrayList<>();
            if (jsonDoors != null) {
                for (int i = 0; i < jsonDoors.length(); i++) {
                    try {
                        doors.add(jsonDoors.getString(i));
                   //     doors.add(Door.fromJsonString(jsonDoors.getJSONObject(i).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            return new Token(name, validity, doors,identifier);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TokenValidity getValidity() {
        return validity;
    }

    public void setValidity(TokenValidity validity) {
        this.validity = validity;
    }

    public List<String> getDoors() {
        return doors;
    }

    public void setDoors(List<String> doors) {
        this.doors = doors;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
