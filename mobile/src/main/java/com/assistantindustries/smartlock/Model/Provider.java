package com.assistantindustries.smartlock.Model;

import com.assistantindustries.smartlock.Tools.JsonTools;
import com.orm.SugarRecord;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alvaro on 26/09/2015.
 */
public class Provider extends SugarRecord<Provider> {
    public static final String JSON_NAME = "name";
    public static final String JSON_PROFILE_PIC_URL = "profilePic";
    public static final String JSON_URL = "url";
    public static final String JSON_IDENTIFIER = "_id";

    String name;
    String url;
    String profilePicUrl;
    String identifier;

    public Provider() {
    }

    public Provider(String name, String url, String profilePicUrl, String identifier) {
        this.name = name;
        this.url = url;
        this.profilePicUrl = profilePicUrl;
        this.identifier = identifier;
    }

    public static Provider fromJson(JSONObject json) {
        Provider provider = new Provider();
        provider.setName(JsonTools.getStringIgnoringException(json, JSON_NAME));
        provider.setProfilePicUrl(JsonTools.getStringIgnoringException(json, JSON_PROFILE_PIC_URL));
        provider.setUrl(JsonTools.getStringIgnoringException(json, JSON_URL));
        provider.setIdentifier(JsonTools.getStringIgnoringException(json, JSON_IDENTIFIER));
        return provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try {
            object.put(JSON_NAME, name);
            object.put(JSON_PROFILE_PIC_URL, profilePicUrl);
            object.put(JSON_URL, url);
            object.put(JSON_IDENTIFIER, identifier);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

}
