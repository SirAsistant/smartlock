package com.assistantindustries.smartlock.Model;

import com.orm.SugarRecord;

import java.util.List;

/**
 * Created by alvaro on 27/09/2015.
 */
public class ProviderCredentials extends SugarRecord<ProviderCredentials> {
    String token;
    String identifier;
    String providerId;
    String providerName;

    public ProviderCredentials() {
    }

    public ProviderCredentials(String token, String identifier, String providerId,String providerName) {
        this.token = token;
        this.identifier = identifier;
        this.providerId = providerId;
        this.providerName=providerName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public static ProviderCredentials findByProviderName(String providerName) {
        return findByProviderName(providerName, listAll(ProviderCredentials.class));
    }

    public static ProviderCredentials findByProviderName(String providerName, List<ProviderCredentials> list) {
        for(ProviderCredentials credentials:list){
            if(credentials.getProviderName().equals(providerName))
                return credentials;
        }
        return null;
    }
}
