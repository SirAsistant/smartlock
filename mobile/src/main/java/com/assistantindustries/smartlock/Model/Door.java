package com.assistantindustries.smartlock.Model;

import com.assistantindustries.smartlock.Tools.JsonTools;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alvaro on 27/09/2015.
 */
public class Door {
    String name;
    String section;
    boolean active;
    boolean online;


    public Door(String name, String section, boolean active, boolean online) {
        this.name = name;
        this.section = section;
        this.active = active;
        this.online = online;
    }

    public static Door fromJsonString(String jsonString) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonObject != null) {
            String name = JsonTools.getStringIgnoringException(jsonObject, "name");
            String section = JsonTools.getStringIgnoringException(jsonObject, "section");
            boolean active = JsonTools.getBooleanIgnoringException(jsonObject, "active");
            boolean online = JsonTools.getBooleanIgnoringException(jsonObject, "online");
            return new Door(name, section, active, online);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
