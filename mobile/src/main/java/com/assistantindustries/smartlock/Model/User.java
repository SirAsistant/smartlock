package com.assistantindustries.smartlock.Model;

import com.assistantindustries.smartlock.Tools.JsonTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvaro on 27/09/2015.
 */
public class User {
    public static final String JSON_ALIAS = "alias";
    public static final String JSON_NAME = "name";
    public static final String JSON_PASSWORD = "password";
    public static final String JSON_TOKEN = "token";
    public static final String JSON_TOKENS = "tokens";
    public static final String JSON_PROFILEPIC = "profilePic";
    public static final String JSON_ACTIVE = "active";
    public static final String JSON_EMAIL = "email";
    public static final String JSON_IDENTIFIER = "_id";

    String alias;
    String password;
    String name;
    String token;
    String profilePic;
    Boolean active;
    String email;
    String identifier;
    List<Token> tokens;

    public User() {
    }

    public User(String alias, String password, String name, String token, String profilePic, Boolean active, String email, List<Token> tokens, String identifier) {
        this.alias = alias;
        this.password = password;
        this.name = name;
        this.token = token;
        this.profilePic = profilePic;
        this.active = active;
        this.email = email;
        this.tokens = tokens;
        this.identifier = identifier;
    }

    public static User fromJsonString(String jsonString) {
        if (jsonString == null)
            return null;
        JSONObject object = null;
        try {
            object = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (object != null) {
            String alias = JsonTools.getStringIgnoringException(object, JSON_ALIAS);
            String password = JsonTools.getStringIgnoringException(object, JSON_PASSWORD);
            String name = JsonTools.getStringIgnoringException(object, JSON_NAME);
            String token = JsonTools.getStringIgnoringException(object, JSON_TOKEN);
            String profilePic = JsonTools.getStringIgnoringException(object, JSON_PROFILEPIC);
            String email = JsonTools.getStringIgnoringException(object, JSON_EMAIL);
            String identifier = JsonTools.getStringIgnoringException(object, JSON_IDENTIFIER);
            boolean active = JsonTools.getBooleanIgnoringException(object, JSON_ACTIVE);
            JSONArray jsonTokens = JsonTools.getJSONArrayIgnoringException(object, JSON_TOKENS);
            List<Token> tokens = new ArrayList<>();
            if (jsonTokens != null) {
                for (int i = 0; i < jsonTokens.length(); i++) {
                    try {
                        JSONObject jsonToken = jsonTokens.getJSONObject(i);
                        Token tokenItem = Token.fromJsonString(jsonToken.toString());
                        tokens.add(tokenItem);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            return new User(alias, password, name, token, profilePic, active, email, tokens, identifier);
        }
        return null;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
