package com.assistantindustries.smartlock;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.assistantindustries.smartlock.Adapters.TokensAdapter;
import com.assistantindustries.smartlock.Model.Token;
import com.assistantindustries.smartlock.View.ScrollTabHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asistant on 29/09/2015.
 */
public class ProviderTokensFragment extends Fragment implements AbsListView.OnScrollListener, ScrollTabHolder {
    private static final String ARG_POSITION = "position";
    private ScrollTabHolder mScrollTabHolder;
    private ListView mListView;
    private int mPosition;
    private TokensAdapter mTokensAdapter;

    public static Fragment newInstance(int position) {
        ProviderTokensFragment f = new ProviderTokensFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_provider_tokens, container, false);
        View placeHolderView = inflater.inflate(R.layout.placeholder_provider_header, mListView, false);

        mListView.setOnScrollListener(this);
        return rootView;
    }

    public void setScrollTabHolder(ScrollTabHolder scrollTabHolder) {
        mScrollTabHolder = scrollTabHolder;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public void adjustScroll(int scrollHeight) {
        if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
            return;
        }
        mListView.setSelectionFromTop(1, scrollHeight);

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {
        onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mScrollTabHolder != null)
            mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // nothing
    }

    public ListView getListView() {
        return mListView;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void setTokens(List<Token> tokens){
        mTokensAdapter.clear();
        mTokensAdapter.addAll(tokens);
        mTokensAdapter.notifyDataSetChanged();
    }
}
