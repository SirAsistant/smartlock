package com.assistantindustries.smartlock;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.assistantindustries.smartlock.Listeners.OnLoginAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnRegisterAttemptedListener;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tasks.LoginTask;
import com.assistantindustries.smartlock.Tasks.RegisterTask;
import com.assistantindustries.smartlock.Tools.Tools;
import com.assistantindustries.smartlock.Tools.UriResolver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.TextViewValidationFailedRenderer;
import ua.org.zasadnyy.zvalidations.validations.ConfirmPassword;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

import static android.app.Activity.RESULT_OK;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProviderAuthActivityFragment extends Fragment {
    public static final int SELECT_PICTURE = 0;
    private static final int CROP_PIC_REQUEST_CODE = 1;

    private Provider mAuthTarget;
    private boolean loginMode = false;

    private View loginView;
    private View registerView;
    private EditText loginUsernameEdit;
    private EditText loginPasswordEdit;
    private EditText usernameEdit;
    private EditText nameEdit;
    private EditText mailEdit;
    private EditText passwordEdit;
    private EditText confirmPasswordEdit;
    private Button registerButton;
    private Button loginButton;
    private View toLoginText;
    private View toRegisterText;
    private ImageButton profilePic;
    private Form loginForm;
    private Form registerForm;

    private Uri photoUri;

    public ProviderAuthActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_provider_auth, container, false);
        try {
            mAuthTarget = Provider.fromJson(new JSONObject(getActivity().getIntent().getExtras().getString(AddProviderActivity.EXTRA_PROVIDER)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        findViews(rootView);
        attachClickListeners();

        loginForm = new Form(getContext());
        loginForm.addField(Field.using(loginUsernameEdit).validate(NotEmpty.build(getContext())));
        loginForm.addField(Field.using(loginPasswordEdit).validate(NotEmpty.build(getContext())));
        loginForm.setValidationFailedRenderer(new TextViewValidationFailedRenderer(getContext()));

        registerForm=new Form(getContext());
        registerForm.addField(Field.using(usernameEdit).validate(NotEmpty.build(getContext())));
        registerForm.addField(Field.using(nameEdit).validate(NotEmpty.build(getContext())));
        registerForm.addField(Field.using(passwordEdit).validate(NotEmpty.build(getContext())));
        registerForm.addField(Field.using(confirmPasswordEdit).validate(ConfirmPassword.build(getContext(), Field.using(passwordEdit))));
        registerForm.addField(Field.using(mailEdit).validate(IsEmail.build(getContext())));
        registerForm.setValidationFailedRenderer(new TextViewValidationFailedRenderer(getContext()));

        if (mAuthTarget == null) {
            getActivity().finish();
        } else {
            updateViews();
        }
        profilePic.setImageResource(R.drawable.profile);
        return rootView;
    }

    private void attachClickListeners() {
        toLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginMode = true;
                updateViews();
            }
        });
        toRegisterText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginMode = false;
                updateViews();
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alias = usernameEdit.getText().toString().trim();
                String password = passwordEdit.getText().toString().trim();
                String confirmPassword = confirmPasswordEdit.getText().toString().trim();
                String mail = mailEdit.getText().toString().trim();
                String name = nameEdit.getText().toString().trim();

                if (registerForm.isValid()) {
                    User user = new User();
                    user.setAlias(alias);
                    user.setPassword(Tools.sha256(password).toUpperCase());
                    user.setName(name);
                    user.setEmail(mail);

                    InputStream photoStream = null;

                    if (photoUri == null) {
                        photoStream = getResources().openRawResource(R.raw.profile);
                    } else {
                        try {
                            String path = UriResolver.getPath(getContext(), photoUri);
                            if (path != null)
                                photoStream = new FileInputStream(new File(path));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    if (photoStream != null) {

                        RegisterTask task = new RegisterTask(mAuthTarget.getUrl(), photoStream, user, new OnRegisterAttemptedListener() {
                            @Override
                            public void onRegisterAttempted(boolean attempted, boolean success, User user, String error) {
                                if (success) {
                                    Intent i = new Intent();
                                    i.putExtra(AddProviderActivity.EXTRA_USER_ID, user.getIdentifier());
                                    i.putExtra(AddProviderActivity.EXTRA_USER_TOKEN, user.getToken());
                                    i.putExtra(AddProviderActivity.EXTRA_PROVIDER, mAuthTarget.toJson().toString());
                                    getActivity().setResult(RESULT_OK, i);
                                    getActivity().finish();
                                } else {
                                    Snackbar.make(loginView, getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();
                                }
                            }
                        });
                        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alias = loginUsernameEdit.getText().toString();
                String password = loginPasswordEdit.getText().toString();

                if (loginForm.isValid()) {
                    LoginTask task = new LoginTask(mAuthTarget.getUrl(), new OnLoginAttemptedListener() {
                    @Override
                    public void onLoginAttempted(boolean attempted, boolean success, User user, String error) {
                        if (attempted&&success) {
                            Intent i = new Intent();
                            i.putExtra(AddProviderActivity.EXTRA_USER_ID, user.getIdentifier());
                            i.putExtra(AddProviderActivity.EXTRA_USER_TOKEN, user.getToken());
                            i.putExtra(AddProviderActivity.EXTRA_PROVIDER, mAuthTarget.toJson().toString());
                            getActivity().setResult(RESULT_OK, i);
                            getActivity().finish();
                        } else {
                            Snackbar.make(loginView, getString(R.string.wrong_username_or_password), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    }}, loginUsernameEdit.getText().toString(), Tools.sha256(loginPasswordEdit.getText().toString()));
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);
            }
        });
    }

    private void updateViews() {
        loginView.setVisibility(loginMode ? View.VISIBLE : View.GONE);
        registerView.setVisibility(loginMode ? View.GONE : View.VISIBLE);
        ( (AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(loginMode? R.string.login:R.string.register));
    }

    private void findViews(View rootView) {
        loginUsernameEdit = (EditText) rootView.findViewById(R.id.user_login_id);
        usernameEdit = (EditText) rootView.findViewById(R.id.user_id);
        loginPasswordEdit = (EditText) rootView.findViewById(R.id.user_login_pass);
        passwordEdit = (EditText) rootView.findViewById(R.id.user_pass);
        confirmPasswordEdit = (EditText) rootView.findViewById(R.id.confirm_pass);
        loginView = rootView.findViewById(R.id.login_container);
        registerView = rootView.findViewById(R.id.register_container);
        registerButton = (Button) rootView.findViewById(R.id.register_button);
        loginButton = (Button) rootView.findViewById(R.id.login_button);
        toLoginText = rootView.findViewById(R.id.to_login_text);
        toRegisterText = rootView.findViewById(R.id.to_register_text);
        profilePic = (ImageButton) rootView.findViewById(R.id.profile_image);
        nameEdit= (EditText) rootView.findViewById(R.id.name);
        mailEdit= (EditText) rootView.findViewById(R.id.mail);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            switch (requestCode) {
                case SELECT_PICTURE:
                    if(resultCode==RESULT_OK){
                        Uri selectedImageUri = data.getData();
                        Intent i = new Intent(getActivity(), CropImageActivity.class);
                        i.putExtra("IMAGE_URI", selectedImageUri);
                        i.putExtra("OUTPUT_PATH", getTemporalCropPath());
                        startActivityForResult(i, CROP_PIC_REQUEST_CODE);
                    }
                    break;
                case CROP_PIC_REQUEST_CODE:
                    if(resultCode==RESULT_OK){
                        Bitmap cropped = BitmapFactory.decodeFile(getTemporalCropPath());
                        profilePic.setImageBitmap(cropped);
                        this.photoUri=getTemporalCropUri();
                    }
                    break;
            }
        }
    }

    private Uri getTemporalCropUri() {
        return Uri.fromFile(new File(getActivity().getCacheDir(), "profilePic"));
    }

    private String getTemporalCropPath() {
        return new File(getActivity().getCacheDir(), "profilePic").getPath();
    }

}
