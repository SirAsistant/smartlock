package com.assistantindustries.smartlock.Tools;

import com.assistantindustries.smartlock.Model.Provider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alvaro on 26/09/2015.
 */
public class JsonTools {
    public static String getStringIgnoringException(JSONObject json, String name) {
        try {
            return json.getString(name);
        } catch (JSONException ignored) {
            return null;
        }
    }

    public static int getIntegerIgnoringException(JSONObject json, String name) {
        try {
            return json.getInt(name);
        } catch (JSONException ignored) {
            return 0;
        }
    }

    public static boolean getBooleanIgnoringException(JSONObject json, String name) {
        try {
            return json.getBoolean(name);
        } catch (JSONException ignored) {
            return false;
        }
    }

    public static JSONArray getJSONArrayIgnoringException(JSONObject json, String name) {
        try {
            return json.getJSONArray(name);
        } catch (JSONException ignored) {
            return null;
        }
    }

    public static JSONObject getJSONObjectIgnoringException(JSONObject json, String name) {
        try {
            return json.getJSONObject(name);
        } catch (JSONException ignored) {
            return null;
        }
    }

    public static ArrayList<String> toStringArray(JSONArray json) {
        ArrayList<String> result = new ArrayList<String>();
        if (json != null) {
            for (int i = 0; i < json.length(); i++) {
                try {
                    result.add(json.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;

    }

    public static Date getDateIgnoringException(String dateStr) {
        try {
            return Iso8601.parse(dateStr, true);
        } catch (Exception e) {
            //Ignore
        }
        return null;
    }

    public static List<Provider> getProviderArray(JSONArray array) {
        ArrayList<Provider> providers = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            try {
                providers.add(Provider.fromJson(array.getJSONObject(i)));
            } catch (JSONException ignored) {
            }
        }

        return providers;
    }

}
