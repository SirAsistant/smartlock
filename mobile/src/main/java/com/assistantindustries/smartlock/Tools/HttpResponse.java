package com.assistantindustries.smartlock.Tools;

/**
 * Created by Asistant on 05/06/2015.
 */
public class HttpResponse {
    private String response;
    private String error;
    private int resultCode;
    private boolean achieved;

    public HttpResponse(String response, String error, int resultCode, boolean achieved) {
        this.response = response;
        this.error = error;
        this.resultCode = resultCode;
        this.achieved = achieved;
    }

    public String getResponseString() {
        return response;
    }

    public String getError() {
        return error;
    }

    public int getResultCode() {
        return resultCode;
    }

    public boolean isAchieved() {
        return achieved;
    }
}
