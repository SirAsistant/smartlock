package com.assistantindustries.smartlock.Tools;

/**
 * Created by assistant on 12/9/16.
 */
public class AsyncParallel {
    private int size;
    private boolean error = false;
    private int finished = 0;
    private FinishCallback finishCallback;


    public void notifyFinish(boolean error){
        this.error |= error;
        this.finished++;
        if(this.finishCallback!=null&&this.finished>=this.size){
          this.finishCallback.onEnd(this.error);
        }
    }


    public void start(int size,FinishCallback finishCallback){
        this.size = size;
        this.finishCallback = finishCallback;
        if(this.finishCallback!=null&&this.finished>=this.size){
            this.finishCallback.onEnd(this.error);
        }
    }

    public interface FinishCallback{
        void onEnd(boolean error);
    }
}
