package com.assistantindustries.smartlock.Tools;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asistant on 15/04/2015.
 */
public class RestClient {
    public static final String POST_METHOD = "POST";
    public static final String GET_METHOD = "GET";

    private HttpURLConnection connection;
    private String url;
    private String method;
    private ArrayList<Header> headers;
    private String body;
    private HttpResponse response;

    public RestClient(String url, String method, String body) {
        this(url, method);
        if (body != null)
            this.body = body;
    }

    public RestClient(String url, String method) {
        this.url = url;
        this.method = method;
        this.body = "";
        this.headers = new ArrayList<>();
    }

    public static String getNameValuePairBody(List<NameValuePair> params)
            throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public void execute() {
        String responseString = "";
        String errorString = "";
        boolean achieved = false;
        int resultCode = -1;
        try {
            URL url = new URL(this.url);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            for (Header header : headers) {
                connection.setRequestProperty(header.getKey(), header.getValue());
            }
            connection.setConnectTimeout(1500);
            connection.setReadTimeout(1500);
            connection.setUseCaches(false);
            connection.setDoInput(true);
            if (method.equals(POST_METHOD)) {
                //Send data if needed
                connection.setDoOutput(true);
                connection.connect();
                DataOutputStream wr = new DataOutputStream(
                        connection.getOutputStream());
                wr.write(body.getBytes("UTF-8"));
                wr.flush();
                wr.close();
            }

            //Get Response
            resultCode = connection.getResponseCode();
            responseString = getAsString(connection.getInputStream());
            achieved = true;
        } catch (Exception e) {
            InputStream errorStream = connection.getErrorStream();
            if (errorStream != null)
                errorString = getAsString(errorStream);
            e.printStackTrace();
            achieved = false;
        } finally {
            response = new HttpResponse(responseString, errorString, resultCode, achieved);
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private String getAsString(InputStream is) {
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String line;
        StringBuilder response = new StringBuilder();
        try {
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    public HttpResponse getResponse() {
        return this.response;
    }

    public void addHeader(String key, String value) {
        headers.add(new Header(key, value));
    }

    private class Header {
        private String key;
        private String value;

        public Header(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

}

