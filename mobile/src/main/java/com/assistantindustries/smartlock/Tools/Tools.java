package com.assistantindustries.smartlock.Tools;

import android.graphics.Bitmap;
import android.graphics.Color;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by alvaro on 26/09/2015.
 */
public class Tools {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static int[] getRefreshColors() {
        return new int[]{Color.parseColor("#ffa700"), Color.parseColor("#008744"), Color.parseColor("#0057e7"), Color.parseColor("#d62d20")};
    }

    public static void addJsonHeaders(RestClient client) {
        client.addHeader("Content-Type", "application/json");
    }

    public static String sha256(String input) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            md.update(input.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Tools.bytesToHex(md.digest());
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static boolean saveBitmap(String path,Bitmap toSave){
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(path);
            toSave.compress(Bitmap.CompressFormat.PNG, 100, out);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean validString(String s) {
        return s != null && !s.isEmpty();
    }

    public static int shiftInteger(int target, int shifts, int min, int max) {
        target+=shifts;
        target=target%max;
        while(target<min){
            target+=max;
        }
        return target;
    }

    public static String joinStrings(String[] list,String delimiter){
        StringBuilder sb = new StringBuilder();
        for (String n : list) {
            if (sb.length() > 0) sb.append(delimiter);
            sb.append(n);
        }
        return sb.toString();
    }

    public static String capitalize(String target) {
        return target.substring(0, 1).toUpperCase() + target.substring(1);
    }
}
