package com.assistantindustries.smartlock;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.assistantindustries.smartlock.Listeners.OnEditUserAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnGetUserAttemptedListener;
import com.assistantindustries.smartlock.Listeners.OnProvidersRetrievedListener;
import com.assistantindustries.smartlock.Model.Provider;
import com.assistantindustries.smartlock.Model.ProviderCredentials;
import com.assistantindustries.smartlock.Model.User;
import com.assistantindustries.smartlock.Tasks.EditUserTask;
import com.assistantindustries.smartlock.Tasks.GetProvidersTask;
import com.assistantindustries.smartlock.Tasks.GetUserTask;
import com.assistantindustries.smartlock.Tools.UriResolver;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import ua.org.zasadnyy.zvalidations.Field;
import ua.org.zasadnyy.zvalidations.Form;
import ua.org.zasadnyy.zvalidations.TextViewValidationFailedRenderer;
import ua.org.zasadnyy.zvalidations.validations.IsEmail;
import ua.org.zasadnyy.zvalidations.validations.NotEmpty;

import static android.app.Activity.RESULT_OK;

/**
 * A placeholder fragment containing a simple view.
 */
public class EditProfileActivityFragment extends Fragment {
    public static final String PROVIDER_NAME = "provider name";
    public static final int SELECT_PICTURE = 1;
    private static final int CROP_PIC_REQUEST_CODE = 2;

    private String mProviderName;
    private ProviderCredentials mCredentials;
    private Provider mProvider;
    private User mUser;
    private Uri photoUri;

    //Views
    private EditText usernameEdit;
    private EditText nameEdit;
    private EditText mailEdit;
    private ImageButton profilePic;
    private Form registerForm;
    private FloatingActionButton fab;

    public EditProfileActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        findViews(rootView);

        mProviderName = getActivity().getIntent().getStringExtra(PROVIDER_NAME);
        if (mProviderName == null) {
            mProviderName = savedInstanceState.getString(PROVIDER_NAME);
        }


        registerForm = new Form(getContext());
        registerForm.addField(Field.using(nameEdit).validate(NotEmpty.build(getContext())));
        registerForm.addField(Field.using(mailEdit).validate(IsEmail.build(getContext())));
        registerForm.setValidationFailedRenderer(new TextViewValidationFailedRenderer(getContext()));

        loadCredentials();
        loadProviderInfo();

        return rootView;
    }


    private void findViews(View rootView) {
        usernameEdit = (EditText) rootView.findViewById(R.id.user_id);
        profilePic = (ImageButton) rootView.findViewById(R.id.profile_image);
        nameEdit = (EditText) rootView.findViewById(R.id.name);
        mailEdit = (EditText) rootView.findViewById(R.id.mail);
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
    }

    private void loadCredentials() {
        List<ProviderCredentials> addedProvidersCredentials = ProviderCredentials.listAll(ProviderCredentials.class);
        for (ProviderCredentials credentials : addedProvidersCredentials) {
            if (credentials.getProviderName().equals(mProviderName))
                mCredentials = credentials;
        }
    }

    private void loadProviderInfo() {
        GetProvidersTask task = new GetProvidersTask(new OnProvidersRetrievedListener() {
            @Override
            public void handleRetrievedProviders(List<Provider> providers) {
                if (providers != null) {
                    for (final Provider provider : providers) {
                        if (provider.getName().equals(mProviderName)) {
                            mProvider = provider;
                            loadUserInfo(provider);
                            break;
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.connection_with_server_failed_try_again_later), Toast.LENGTH_LONG).show();
                }
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadUserInfo(Provider provider) {
        GetUserTask getUserTask = new GetUserTask(provider.getUrl(), mCredentials.getIdentifier(), new OnGetUserAttemptedListener() {
            @Override
            public void onGetUserAttempted(boolean connected,boolean achieved, User user) {
                if (achieved && user != null) {
                    mUser = user;
                    fillFormData();
                    enableSubmit();
                    Picasso.with(getActivity()).load(mUser.getProfilePic()).into(profilePic, new Callback() {
                        @Override
                        public void onSuccess() {
                            profilePic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent,
                                            "Select Picture"), SELECT_PICTURE);
                                }
                            });
                        }

                        @Override
                        public void onError() {
                            Snackbar.make(profilePic, getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), getString(R.string.connection_with_server_failed_try_again_later), Toast.LENGTH_LONG).show();
                }
            }
        });
        getUserTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void enableSubmit() {
        fab.setVisibility(View.VISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (registerForm.isValid()) {
                    final User user = new User();
                    user.setName(nameEdit.getText().toString().trim());
                    user.setEmail(mailEdit.getText().toString().trim());
                    user.setIdentifier(mCredentials.getIdentifier());

                    InputStream photoStream = null;
                    if(photoUri!=null){
                        try {
                            String path = UriResolver.getPath(getContext(), photoUri);
                            if (path != null)
                                photoStream = new FileInputStream(new File(path));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    EditUserTask getUserTask = new EditUserTask(mProvider.getUrl(), photoStream, user, new OnEditUserAttemptedListener() {
                        @Override
                        public void onEditUserAttempted(boolean attempted, boolean success) {
                            if (attempted && success) {
                                Snackbar.make(v, getString(R.string.changes_saved), Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            } else {
                                Snackbar.make(v, getString(R.string.connection_with_server_failed_try_again_later), Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();
                            }
                        }
                    });
                    getUserTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
    }

    private void fillFormData() {
        usernameEdit.setText(mUser.getAlias());
        nameEdit.setText(mUser.getName());
        mailEdit.setText(mUser.getEmail());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECT_PICTURE:
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    Intent i = new Intent(getActivity(), CropImageActivity.class);
                    i.putExtra("IMAGE_URI", selectedImageUri);
                    i.putExtra("OUTPUT_PATH", getTemporalCropPath());
                    startActivityForResult(i, CROP_PIC_REQUEST_CODE);
                }
                break;
            case CROP_PIC_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Bitmap cropped = BitmapFactory.decodeFile(getTemporalCropPath());
                    profilePic.setImageBitmap(cropped);
                    this.photoUri = getTemporalCropUri();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private Uri getTemporalCropUri() {
        return Uri.fromFile(new File(getActivity().getCacheDir(), "profilePic"));
    }

    private String getTemporalCropPath() {
        return new File(getActivity().getCacheDir(), "profilePic").getPath();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(PROVIDER_NAME, mProviderName);
        super.onSaveInstanceState(outState);
    }
}
