package com.assistantindustries.smartlock;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.FileOutputStream;
import java.io.IOException;


public class CropImageActivity extends AppCompatActivity {

    private CropImageView cropImageView;
    private String outputPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);
        cropImageView = (CropImageView) findViewById(R.id.crop_view);
        Uri imageUri = getIntent().getParcelableExtra("IMAGE_URI");
        outputPath = getIntent().getExtras().getString("OUTPUT_PATH", "");
        cropImageView.setImageUriAsync(imageUri);
        cropImageView.setAspectRatio(1,1);
        cropImageView.setScaleType(com.theartofdev.edmodo.cropper.CropImageView.ScaleType.FIT_CENTER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crop_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    public void rotateLeft(View view) {
        cropImageView.rotateImage(-90);
    }

    public void rotateRight(View view) {
        cropImageView.rotateImage(90);
    }

    public void cancel(View view) {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void confirm(View view) {
        Intent i = new Intent();
        Bitmap bitmap = cropImageView.getCroppedImage(500, 500);
        if (outputPath.isEmpty()) {
            i.putExtra("OUTPUT_IMAGE", bitmap);
        } else {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(outputPath);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        setResult(RESULT_OK, i);
        finish();
    }
}
